package GoncaloCFerreira.demo;

import GoncaloCFerreira.demo.Controller.AverageController;
import GoncaloCFerreira.demo.Controller.RequestController;
import GoncaloCFerreira.demo.Utils.LoggingHandler;
import GoncaloCFerreira.demo.Utils.PropertiesReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Properties;

/**
 * Main Application Class
 * <p>
 * Contains all the endpoint accesses.
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class})
@RestController
public class FlightInPortugalApplication {

    private static final AverageController averageController = new AverageController();
    private static final RequestController requestController = new RequestController();

    public static void main(String[] args) {
        System.setProperty("server.servlet.context-path", "/flight");
        SpringApplication.run(FlightInPortugalApplication.class, args);
    }

    /**
     * Used to run the application in a different environment, using a different port
     */
    public static void run(String[] args, String env) {
        if (env.equals("TEST")) {
            try {
                Properties properties = PropertiesReader.readPropertiesFile("application.properties");
                String port = properties.getProperty("TEST_SERVER_PORT");
                System.setProperty("server.port", port);
            } catch (IOException e) {
                LoggingHandler.logger.error("Properties file not found!");
            }
        }
        SpringApplication.run(FlightInPortugalApplication.class, args);
    }

    /**
     * /avg endpoint, of which the result contains:
     * - Currency
     * - From
     * - To
     * - dateFrom
     * - dateTo
     * - price_average
     * - numberOfFlights
     * - bag1_average
     * - bag2_average
     *
     * @param from     Origin code for the flight
     * @param to       Destiny code for the flight
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     * @param request  Request of the Client
     * @return JSON String
     */
    @RequestMapping(value = "/avg", produces = {"application/json"}, method = RequestMethod.GET)
    public String avg(
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to,
            @RequestParam(value = "dateFrom") String dateFrom,
            @RequestParam(value = "dateTo") String dateTo,
            HttpServletRequest request
    ) {
        LoggingHandler.logger.warn("IP: " + request.getRemoteAddr() + " requested /avg");
        return averageController.getAveragePricesResult(request, from, to, dateFrom, dateTo);
    }

    /**
     * /list endpoint, lists all the requests in the MongoDB database
     *
     * @param request Request of the Client
     * @return JSON String
     */
    @RequestMapping(value = "/list", produces = {"application/json"}, method = RequestMethod.GET)
    public String list(HttpServletRequest request) {
        LoggingHandler.logger.warn("IP: " + request.getRemoteAddr() + " requested /list");
        return requestController.getRequestList(request);
    }

    /**
     * /list endpoint, lists all the requests in the MongoDB database
     *
     * @param request Request of the Client
     * @return JSON String
     */
    @RequestMapping(value = "/deleteAll", produces = {"application/json"}, method = RequestMethod.DELETE)
    public String deleteAll(HttpServletRequest request) {
        LoggingHandler.logger.warn("IP: " + request.getRemoteAddr() + " requested /deleteAll");
        return requestController.deleteRequestList(request);
    }

    /**
     * ExceptionHandler that handles missing parameters
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public String handleMissingParams(MissingServletRequestParameterException ex) {
        String name = ex.getParameterName();
        if (name.equals("dateFrom") || name.equals("dateTo")) {
            return name + " parameter is missing (please use the format: dd/mm/yyyy)";
        }
        return name + " parameter is missing";
    }

    /**
     * Endpoint for unknown endpoint requests
     */
    @GetMapping("/error")
    public String error(HttpServletRequest request) {
        LoggingHandler.logger.warn("IP: " + request.getRemoteAddr() + " tried to access a non-existent endpoint");
        return "Unknown endpoint. Please use /avg";
    }
}
