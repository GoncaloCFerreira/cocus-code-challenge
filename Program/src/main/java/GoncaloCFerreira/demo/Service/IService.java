package GoncaloCFerreira.demo.Service;

/**
 * Repository interface for future implementations of other external API connections
 */
public interface IService {

    String getResponse(String from, String to, String dateFrom, String dateTo);
}
