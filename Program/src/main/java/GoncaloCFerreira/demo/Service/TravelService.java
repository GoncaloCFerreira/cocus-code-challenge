package GoncaloCFerreira.demo.Service;

import GoncaloCFerreira.demo.Utils.LoggingHandler;
import GoncaloCFerreira.demo.Utils.PropertiesReader;
import GoncaloCFerreira.demo.Utils.ValueUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Properties;

@Service
public class TravelService implements IService {

    private final RestTemplate restTemplate = new RestTemplate();

    private static String DEFAULT_FROM;
    private static String DEFAULT_TO;
    private static String DEFAULT_HEAD;
    private static String DEFAULT_AIRLINES;
    private static String DEFAULT_PARTNER;
    private static String DEFAULT_CURRENCY;

    /**
     * Flags to be appended to the external API's JSON string.
     * Indicating if any of these four attributes was not used in the request
     */
    private static String fromFlag;
    private static String toFlag;
    private static String dateFromFlag;
    private static String dateToFlag;

    /**
     * Contacts the external API to gather the flight data
     * <p>
     * The default values are all read from the application.properties file, located at src/main/resources
     */
    public TravelService() {
        try {
            Properties prop = PropertiesReader.readPropertiesFile("application.properties");
            DEFAULT_FROM = prop.getProperty("DEFAULT_FROM");
            DEFAULT_TO = prop.getProperty("DEFAULT_TO");
            DEFAULT_HEAD = prop.getProperty("DEFAULT_HEAD");
            DEFAULT_AIRLINES = "&select_airlines=" + prop.getProperty("DEFAULT_AIRLINE_1") + "," + prop.getProperty("DEFAULT_AIRLINE_2");
            DEFAULT_PARTNER = "&partner=" + prop.getProperty("DEFAULT_PARTNER");
            DEFAULT_CURRENCY = "&curr=" + prop.getProperty("DEFAULT_CURRENCY");
        } catch (IOException e) {
            LoggingHandler.logger.warn("IOException on the constructor of TravelService");
        }
    }

    /**
     * @param from     Origin code for the flight
     * @param to       Destiny code for the flight
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     * @return JSON String response
     */
    public String getResponse(String from, String to, String dateFrom, String dateTo) {
        String response;
        String url = constructUrl(from, to, dateFrom, dateTo);
        response = contactAPI(url);
        response = addDateAndDestinationsToResponse(response);
        return response;
    }

    /**
     * Contacts the external API, checks if the Https Status is OK and if so gets the body
     *
     * @param url requested URL
     * @return external API's request body
     */
    private String contactAPI(String url) {
        ResponseEntity<String> responseEntity;
        responseEntity = restTemplate.getForEntity(url, String.class);
        return responseEntity.getStatusCode() == HttpStatus.OK ? responseEntity.getBody() : null;
    }

    /**
     * Adds 4 more attributes to the response, in order to have a better output for the user
     *
     * @param response The external API's response
     * @return complete response JSON String
     */
    private String addDateAndDestinationsToResponse(String response) {
        response = response.substring(0, response.length() - 1);
        response += ",\"from\": \"" + fromFlag + "\"";
        response += ",\"to\": \"" + toFlag + "\"";
        response += ",\"dateFrom\": \"" + dateFromFlag + "\"";
        response += ",\"dateTo\": \"" + dateToFlag + "\"}";
        return response;
    }


    /**
     * Constructs the URL using the default parameters
     * (HEAD, AIRLINES, PARTNER and CURRENCY)
     * as well as adding the client's parameters
     * (from, to, dateFrom and dateTo)
     *
     * @param from     Origin code for the flight
     * @param to       Destiny code for the flight
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     * @return The constructed URL
     */
    public String constructUrl(String from, String to, String dateFrom, String dateTo) {
        return DEFAULT_HEAD
                + DEFAULT_AIRLINES
                + constructULRDestination(from, to)
                + constructURLDate(dateFrom, dateTo)
                + DEFAULT_PARTNER
                + DEFAULT_CURRENCY;
    }

    /**
     * Sets the missing parameters as their the default value
     * and notifies their flags
     *
     * @param from Origin code for the flight
     * @param to   Destiny code for the flight
     * @return destination URL component
     */
    public String constructULRDestination(String from, String to) {
        if ((from == null || to == null || from.equals(to))) {
            from = DEFAULT_FROM;
            to = DEFAULT_TO;
            fromFlag = "Default origin: " + from;
            toFlag = "Default destiny: " + to;
        } else {
            fromFlag = from;
            toFlag = to;
        }
        return "&flyFrom=" + from + "&to=" + to;
    }

    /**
     * Sets the missing parameters as their the default value
     * and notifies their flags
     *
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     * @return date URL component
     */
    public String constructURLDate(String dateFrom, String dateTo) {
        String dateUrl = "";

        if (ValueUtils.validateDate(dateFrom)) {
            dateUrl += "&dateFrom=" + dateFrom;
            dateFromFlag = ValueUtils.stringifyDate(dateFrom);
        } else {
            dateFromFlag = "Invalid requested data, ignored. Please use the format: dd/mm/yyyy";
        }

        if (ValueUtils.validateDate(dateTo)) {
            dateUrl += "&dateTo=" + dateTo;
            dateToFlag = ValueUtils.stringifyDate(dateTo);
        } else {
            dateToFlag = "Invalid requested data, ignored. Please use the format: dd/mm/yyyy";
        }
        return dateUrl;
    }

}
