package GoncaloCFerreira.demo.Controller;

import GoncaloCFerreira.demo.Repository.RequestRepository;
import GoncaloCFerreira.demo.Repository.RepositoryFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

public class RequestController {
    private final RequestRepository repository;

    public RequestController() {
        repository = RepositoryFactory.getRepository();
    }

    public String getRequestList(HttpServletRequest request) {
        repository.insertRequest(request, "getRequestList");
        return Objects.requireNonNull(repository.listRequests());
    }

    public String deleteRequestList(HttpServletRequest request) {
        repository.insertRequest(request, "deleteRequestList");
        return Objects.requireNonNull(repository.deleteRequests());
    }
}
