package GoncaloCFerreira.demo.Controller;

import GoncaloCFerreira.demo.Model.IncomingFlightSearch.IncomingFlightSearch;
import GoncaloCFerreira.demo.Model.OutgoingFlightSearch.OutgoingFlightSearch;
import GoncaloCFerreira.demo.Repository.RequestRepository;
import GoncaloCFerreira.demo.Repository.RepositoryFactory;
import GoncaloCFerreira.demo.Utils.FlightSearchJsonMapper;
import GoncaloCFerreira.demo.Service.TravelService;
import GoncaloCFerreira.demo.Utils.LoggingHandler;
import GoncaloCFerreira.demo.Utils.OutgoingFlightSearchBuilder;
import GoncaloCFerreira.demo.Utils.ValueUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller for the Average Use Case.
 */
public class AverageController {
    private final FlightSearchJsonMapper flightMapper;
    private final TravelService service;
    private final RequestRepository repository;

    public AverageController() {
        flightMapper = new FlightSearchJsonMapper();
        service = new TravelService();
        repository = RepositoryFactory.getRepository();
    }

    /**
     * Contructs the result for the client's request in steps.
     * 1 - Registers the Request in the MongoDB database
     * 2 - Validates the Data Format
     * 3 - Contacts the Flight API to gather the information requested
     * 4 - Constructs a Model object with the information gathered from the external API
     * 5 - Constructs a Model object from the information that was precessed from the former Model object.
     * 6 - Constructs the result JSON object with the requested information.
     *
     * @param request  Request information used to inform the database of the request.
     * @param from     Origin code for the flight
     * @param to       Destiny code for the flight
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     * @return JSON Object with the requested information.
     */
    public String getAveragePricesResult(HttpServletRequest request, String from, String to, String dateFrom, String dateTo) {
        repository.insertRequest(request, "getAveragePricesResult");
        if (validateData(dateFrom, dateTo)) {
            String apiResponse = service.getResponse(from, to, dateFrom, dateTo);
            if (apiResponse != null) {
                IncomingFlightSearch flightSearch = constructSearchObject(apiResponse);
                OutgoingFlightSearch resultFlightSearch;
                if (flightSearch != null) {
                    resultFlightSearch = OutgoingFlightSearchBuilder.build(flightSearch);
                    if (resultFlightSearch==null){
                        return "No results where found";
                    }
                } else {
                    LoggingHandler.logger.warn("Failed to construct FlightSearchObject from JSON String");
                    return "Internal error";
                }
                return flightMapper.constructObjectToJson(resultFlightSearch);
            }
            return "No results where found";
        }
        return "Invalid date formats (please use the format: dd/mm/yyyy) ";
    }


    /**
     * Validates both dateFrom and dateTo
     * If any of them is not in the proper format the Controller
     * will return the appropriate message to the client
     *
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     * @return boolean
     */
    private boolean validateData(String dateFrom, String dateTo) {
        return ValueUtils.validateDate(dateFrom) && ValueUtils.validateDate(dateTo);
    }


    /**
     * @param response External API response (JSON format)
     * @return IncomingFlightSearch object, constructed from the JSON response
     */
    private IncomingFlightSearch constructSearchObject(String response) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(response);
            return (IncomingFlightSearch) flightMapper.constructJsonToObject(node);
        } catch (JsonProcessingException e) {
            LoggingHandler.logger.warn("JsonProcessingException occured while constructing the result Json");
        }
        return null;
    }
}
