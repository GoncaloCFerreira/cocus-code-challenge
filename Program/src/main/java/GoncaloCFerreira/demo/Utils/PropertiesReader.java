package GoncaloCFerreira.demo.Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


/**
 * Class in charge of reading the properties file.
 */
public class PropertiesReader {
    public static Properties readPropertiesFile(String fileName) throws IOException {
        FileInputStream fis = null;
        Properties properties = null;
        try {
            fis = new FileInputStream("src/main/resources/" + fileName);
            properties = new Properties();
            properties.load(fis);
        } catch (IOException fnfe) {
            fnfe.printStackTrace();
        } finally {
            assert fis != null;
            fis.close();
        }
        return properties;
    }
}
