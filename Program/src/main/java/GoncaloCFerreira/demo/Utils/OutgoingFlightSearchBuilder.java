package GoncaloCFerreira.demo.Utils;

import GoncaloCFerreira.demo.Model.IncomingFlightSearch.Flight;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.IncomingFlightSearch;
import GoncaloCFerreira.demo.Model.OutgoingFlightSearch.OutgoingFlightSearch;

import java.util.List;

public class OutgoingFlightSearchBuilder {


    /**
     * Builds a OutgoingFlightSearch model object from a IncomingFLightSearch model object.
     *
     * This class makes sure that all the business rules are applied
     * in between the different FlightSearch object States.
     *
     * @param flightSearch
     * @return OutgoingFlightSearch
     */
    public static OutgoingFlightSearch build(IncomingFlightSearch flightSearch) {
        List<Flight> data = flightSearch.getData();
        if(data.size()==0){
            return null;
        }

        double price_average = ValueUtils.round(assertPriceAverage(data));
        double[] bag_average = assertBagAverage(data);
        double bag1_average = ValueUtils.round(bag_average[0]);
        double bag2_average = ValueUtils.round(bag_average[1]);
        int numberOfFlights = data.size();

        return new OutgoingFlightSearch(flightSearch, price_average, bag1_average, bag2_average, numberOfFlights);
    }

    /**
     * Asserts the average of all the first and second bag prices
     * <p>
     * NOTE: Some flights do not have the second bag, and in so wont count
     * for the average of that specific bag price.
     *
     * @param data List of flights
     * @return average
     */
    private static double[] assertBagAverage(List<Flight> data) {
        double totalPrice1 = 0.0;
        double totalPrice2 = 0.0;
        int totalBag1Count = 0;
        int totalBag2Count = 0;
        for (Flight flight : data) {
            double bag1Price = flight.getBag1_price();
            double bag2Price = flight.getBag2_price();
            if (bag1Price != 0.0) {
                totalPrice1 += bag1Price;
                totalBag1Count++;
            }
            if (bag2Price != 0.0) {
                totalPrice2 += bag2Price;
                totalBag2Count++;
            }
        }

        double[] results = new double[2];
        results[0] = totalPrice1 / totalBag1Count;
        results[1] = totalPrice2 / totalBag2Count;
        return results;
    }

    /**
     * Asserts the average of all the flight prices
     *
     * @param data List of flights
     * @return average
     */
    private static double assertPriceAverage(List<Flight> data) {
        double totalPrice = 0.0;
        for (Flight flight : data) {
            totalPrice += flight.getPrice();
        }
        return totalPrice / data.size();
    }

}
