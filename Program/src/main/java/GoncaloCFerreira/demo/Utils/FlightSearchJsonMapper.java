package GoncaloCFerreira.demo.Utils;

import GoncaloCFerreira.demo.Model.FlightSearch;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.Flight;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.IncomingFlightSearch;
import GoncaloCFerreira.demo.Model.OutgoingFlightSearch.OutgoingFlightSearch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FlightSearchJsonMapper {

    /**
     * Constructs a IncomingFlightSearch model object from the JSON string supplied by the external API
     * @param searchJson Input JSON String
     * @return IncomingFlightSearch
     */
    public FlightSearch constructJsonToObject(JsonNode searchJson) {
        String searchId = searchJson.get("search_id").asText();
        String currency = searchJson.get("currency").asText();
        String from = searchJson.get("from").asText();
        String to = searchJson.get("to").asText();
        String dateFrom = searchJson.get("dateFrom").asText();
        String dateTo = searchJson.get("dateTo").asText();

        Iterator<JsonNode> jsonflightList = searchJson.get("data").iterator();

        List<Flight> objFlightList = new ArrayList<>();
        while (jsonflightList.hasNext()) {
            JsonNode flight = jsonflightList.next();

            String flight_id = flight.get("id").asText();
            List<String> flight_airlines = getFlightAirLines(flight);
            String flyFrom = flight.get("flyFrom").asText();
            String flyTo = flight.get("flyTo").asText();
            double price = flight.get("price").asDouble();
            JsonNode bag1 = flight.get("bags_price").get("1");
            JsonNode bag2 = flight.get("bags_price").get("2");
            double bag1_price = 0.0;
            double bag2_price = 0.0;
            if (bag1 != null) {
                bag1_price = bag1.asDouble();
            }
            if (bag2 != null) {
                bag2_price = bag2.asDouble();
            }

            Flight newFlight = new Flight(flight_id, flight_airlines, flyFrom, flyTo, price, bag1_price, bag2_price);
            objFlightList.add(newFlight);
        }

        FlightSearch flightSearch = new FlightSearch(searchId, currency, from, to, dateFrom, dateTo);

        return new IncomingFlightSearch(flightSearch, objFlightList);
    }

    /**
     * Returns all the airlines of the flight
     * @param flight Flight in json format
     * @return List of airline names
     */
    private static List<String> getFlightAirLines(JsonNode flight) {
        Iterator<JsonNode> airlineList = flight.get("airlines").iterator();
        List<String> result = new ArrayList<>();

        while (airlineList.hasNext()) {
            JsonNode airline = airlineList.next();
            result.add(airline.asText());
        }
        return result;
    }


    /**
     * Maps the OutgoingFlightSearch object into a JSON String to serve as the body of the request's answer
     *
     * @param search FlightSearch to be serialized
     * @return JSON String
     */
    public String constructObjectToJson(OutgoingFlightSearch search) {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(search);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

}
