package GoncaloCFerreira.demo.Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;


/**
 * Utilitarian class for values and dates
 */
public class ValueUtils {

    private static final Pattern DATE_PATTERN = Pattern.compile("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$");

    /*
        Rounds a number consistently in the program
     */
    public static double round(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.DOWN).doubleValue();
    }

    /*
        Validates a Date using the regex pattern DATE_PATTERN
     */
    public static boolean validateDate(String date) {
        if (date == null) {
            return false;
        }
        return DATE_PATTERN.matcher(date).matches();
    }

    /*
        Replaces all "/" for "\/" because of JSON limitations.
        Used in dates.
     */
    public static String stringifyDate(String date) {
        return date.replace("/", "\\/");
    }


    /*
     Returns the current date, used as a timeStamp in database requests
     */
    public static String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        return (formatter.format(date));
    }

}
