package GoncaloCFerreira.demo.Utils;

import GoncaloCFerreira.demo.FlightInPortugalApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Static logger to be used in all the classes that need it
 */
public class LoggingHandler {
    public static Logger logger =  LoggerFactory.getLogger(FlightInPortugalApplication.class);
}
