package GoncaloCFerreira.demo.Model.OutgoingFlightSearch;

import GoncaloCFerreira.demo.Utils.ValueUtils;

/**
 * Model representation of the Bags object, which contains the average prices
 * of the first and second bag
 */
public class Bags {
    private final double bag1_average;
    private final double bag2_average;

    public Bags(double bag1_average, double bag2_average) {
        this.bag1_average = ValueUtils.round(bag1_average);
        this.bag2_average = ValueUtils.round(bag2_average);
    }

    public double getBag1_average() {
        return bag1_average;
    }

    public double getBag2_average() {
        return bag2_average;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bags bags = (Bags) o;
        return Double.compare(bags.bag1_average, bag1_average) == 0 &&
                Double.compare(bags.bag2_average, bag2_average) == 0;
    }

}
