package GoncaloCFerreira.demo.Model.OutgoingFlightSearch;

import GoncaloCFerreira.demo.Model.FlightSearch;

/**
 * Model representation of the FlightSearch object that results from the transformation of the IncomingFLightSearch object
 * provided by the external API.
 *
 * This object has the information that is of interest to the Client.
 */
public class OutgoingFlightSearch extends FlightSearch {

    private final Bags bag_prices;
    private final double price_average;
    private final int number_of_flights;

    /**
     * @param flightSearch corresponding FlightSearch
     * @param price_average average of flight prices
     * @param bag1_average average of price of the first bag
     * @param bag2_average average of price of the second bag
     * @param number_of_flights number of flights
     */
    public OutgoingFlightSearch(FlightSearch flightSearch, double price_average, double bag1_average, double bag2_average, int number_of_flights) {
        super(flightSearch);
        this.price_average = price_average;
        this.bag_prices = new Bags(bag1_average, bag2_average);
        this.number_of_flights = number_of_flights;
    }

    public Bags getBagPrices() {
        return bag_prices;
    }

    public double getPrice_average() {
        return price_average;
    }

    public int getNumber_of_flights() {
        return number_of_flights;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutgoingFlightSearch that = (OutgoingFlightSearch) o;
        return Double.compare(that.price_average, price_average) == 0 &&
                number_of_flights == that.number_of_flights &&
                bag_prices.equals(that.bag_prices);
    }

}
