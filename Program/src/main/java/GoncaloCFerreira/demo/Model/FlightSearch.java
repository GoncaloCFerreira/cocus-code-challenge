package GoncaloCFerreira.demo.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Model representation of the FlightSearch object that is used for IncomingFlightSearch and OutgoingFLightSearch.
 * <p>
 * This object contains the common data between the former 2, and is thereby their base class.
 */
public class FlightSearch {

    /**
     * @param search_id ID of the search
     * @param currency Currency used
     * @param from     Origin code for the flight
     * @param to       Destiny code for the flight
     * @param dateFrom First date of the interval of requested Flights
     * @param dateTo   Last date of the interval of requested Flights
     */
    @JsonIgnore
    private final String search_id;
    private final String currency;
    private final String from;
    private final String to;
    private final String dateFrom;
    private final String dateTo;

    public FlightSearch(String search_id, String currency, String from, String to, String dateFrom, String dateTo) {
        this.search_id = search_id;
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public FlightSearch(FlightSearch flightSearch) {
        this.search_id = flightSearch.getSearch_id();
        this.currency = flightSearch.getCurrency();
        this.from = flightSearch.getFrom();
        this.to = flightSearch.getTo();
        this.dateFrom = flightSearch.getDateFrom();
        this.dateTo = flightSearch.getDateTo();
    }

    public String getSearch_id() {
        return search_id;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }
}
