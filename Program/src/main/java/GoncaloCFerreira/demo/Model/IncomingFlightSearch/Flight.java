package GoncaloCFerreira.demo.Model.IncomingFlightSearch;

import java.util.List;
import java.util.Objects;

/**
 * Model representation of the Flight object, constructed from the
 * information returned by the external API
 */
public class Flight {


    /**
     * @param flight_id ID of the flight
     * @param airlines List of Airlines
     * @param from     Origin code for the flight
     * @param to       Destiny code for the flight
     * @param price    Price of the ticket
     * @param bag1_price    Price of the first bag
     * @param bag2_price    Price of the second bag
     */

    private final String flight_id;
    private final List<String> airlines;
    private final String flyFrom;
    private final String flyTo;
    private final double price;
    private final double bag1_price;
    private final double bag2_price;

    public Flight(String id, List<String> airlines, String flyFrom, String flyTo, double price, double bag1_price, double bag2_price) {
        this.flight_id = id;
        this.airlines = airlines;
        this.flyFrom = flyFrom;
        this.flyTo = flyTo;
        this.price = price;
        this.bag1_price = bag1_price;
        this.bag2_price = bag2_price;
    }

    public String getFlight_id() {
        return flight_id;
    }

    public List<String> getAirlines() {
        return airlines;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public double getPrice() {
        return price;
    }

    public double getBag1_price() {
        return bag1_price;
    }

    public double getBag2_price() {
        return bag2_price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equals(flight_id, flight.flight_id);
    }

}
