package GoncaloCFerreira.demo.Model.IncomingFlightSearch;


import GoncaloCFerreira.demo.Model.FlightSearch;

import java.util.List;

/**
 * Model representation of the FlightSearch object that results from the gathering of information
 * provided by the external API
 */
public class IncomingFlightSearch extends FlightSearch {

    /**
     * List of Flights in the search
     */
    private final List<Flight> flightData;

    public IncomingFlightSearch(FlightSearch flightSearch, List<Flight> flightData) {
        super(flightSearch);
        this.flightData = flightData;
    }

    public List<Flight> getData() {
        return flightData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IncomingFlightSearch that = (IncomingFlightSearch) o;
        return flightData.equals(that.flightData);
    }
}
