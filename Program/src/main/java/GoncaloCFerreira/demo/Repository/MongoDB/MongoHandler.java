package GoncaloCFerreira.demo.Repository.MongoDB;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

/**
 * MongoDB handler, responsible for the configuration and creation of the database connection.
 */
public class MongoHandler {

    /**
     * Connection URL.
     */
    private static final ConnectionString connString = new ConnectionString(
            "mongodb+srv://admin:admin@flightrequests.g5vnj.azure.mongodb.net/FlightRequests?retryWrites=true&w=majority"
    );

    /**
     * Connection settings.
     */
    private static final MongoClientSettings settings = MongoClientSettings.builder()
            .applyConnectionString(connString)
            .retryWrites(true)
            .build();

    private static MongoClient mongoClient;

    /**
     * Access to the database.
     */
    public static MongoClient getMongo() {
        if (mongoClient == null) {
            mongoClient = MongoClients.create(settings);
        }
        return mongoClient;
    }
}
