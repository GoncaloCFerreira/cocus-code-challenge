package GoncaloCFerreira.demo.Repository.MongoDB;

import GoncaloCFerreira.demo.Repository.RequestRepository;
import GoncaloCFerreira.demo.Utils.LoggingHandler;
import GoncaloCFerreira.demo.Utils.ValueUtils;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MongoDBRequestRepository implements RequestRepository {
    private static MongoCollection<Document> collection;

    /**
     * Initialization of the MongoDB collection access point.
     */
    public MongoDBRequestRepository() {
        MongoDatabase database = MongoHandler.getMongo().getDatabase("FlightRequests");
        collection = database.getCollection("Requests");
    }

    /**
     * Inserts the request into the MongoDB collection
     */
    public void insertRequest(HttpServletRequest request, String requestType) {
        Document person = new Document("address", request.getRemoteAddr())
                .append("request", requestType)
                .append("timeStamp", ValueUtils.getDate());
        try {
            collection.insertOne(person);
        } catch (MongoWriteException e) {
            LoggingHandler.logger.warn("Error inserting " + person + " into database");

        }
    }

    /**
     * Lists all the requests in the database
     *
     * @return the list or null
     */
    public String listRequests() {
        try {
            return StreamSupport.stream(collection.find().spliterator(), false)
                    .map(Document::toJson)
                    .collect(Collectors.joining(", ", "[", "]"));
        } catch (MongoWriteException e) {
            LoggingHandler.logger.warn("Error listing requests from the database");
        }
        return "Error listing requests from the database";
    }

    /**
     * Deletes all the requests in the database
     */
    public String deleteRequests() {
        try {
            BasicDBObject document = new BasicDBObject();
            var count = collection.deleteMany(document).getDeletedCount();
            return "Deleted with sucess: " + count + " requests";
        } catch (MongoWriteException e) {
            LoggingHandler.logger.warn("Error deleting requests from the database");
        }
        return "Error deleting requests from the database";
    }
}
