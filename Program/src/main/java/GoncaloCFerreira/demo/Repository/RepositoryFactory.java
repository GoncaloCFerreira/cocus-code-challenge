package GoncaloCFerreira.demo.Repository;

import GoncaloCFerreira.demo.Repository.MongoDB.MongoDBRequestRepository;
import GoncaloCFerreira.demo.Utils.LoggingHandler;
import GoncaloCFerreira.demo.Utils.PropertiesReader;

import java.io.IOException;
import java.util.Properties;

/**
 * Repository factory, to add more implementations of AbstractRepository
 * in order to accommodate other types of databases
 */
public class RepositoryFactory {

    public static RequestRepository getRepository() {
        String repositoryType = "";
        try {
            Properties prop = PropertiesReader.readPropertiesFile("application.properties");
            repositoryType = prop.getProperty("DATABASE");
        } catch (IOException e) {
            LoggingHandler.logger.error("No defined database could be found,using Mongodb");
        }
        switch (repositoryType) {
            case "mongodb":
            default: {
                return new MongoDBRequestRepository();
            }
        }
    }
}
