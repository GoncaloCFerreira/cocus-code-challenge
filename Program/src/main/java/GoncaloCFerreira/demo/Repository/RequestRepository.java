package GoncaloCFerreira.demo.Repository;

import javax.servlet.http.HttpServletRequest;

/**
 * Repository interface for future implementations of other database types
 */
public interface RequestRepository {

    void insertRequest(HttpServletRequest request, String requestType);

    String listRequests();

    String deleteRequests();

}
