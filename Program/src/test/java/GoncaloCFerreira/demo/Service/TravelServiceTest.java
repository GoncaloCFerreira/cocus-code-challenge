package GoncaloCFerreira.demo.Service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(MockitoJUnitRunner.class)
class TravelServiceTest {

    TravelService service = new TravelService();

    private final String from = "BAR";
    private final String to = "PAR";
    private final String dateFrom = "18/11/2020";
    private final String dateTo = "12/12/2021";

    private static final String DEFAULT_FROM = "OPO";
    private static final String DEFAULT_TO = "LIS";

    @Test
    void constructULRDestinationValid() {
        String result = service.constructULRDestination(from, to);
        String expected = "&flyFrom=" + from + "&to=" + to;
        assertEquals(result, expected);
    }

    @Test
    void constructULRDestinationInvalidFrom() {
        String result = service.constructULRDestination(DEFAULT_FROM, to);
        String expected = "&flyFrom=" + DEFAULT_FROM + "&to=" + to;
        assertEquals(expected,result);
    }

    @Test
    void constructULRDestinationInvalidTo() {
        String result = service.constructULRDestination(from, DEFAULT_TO);
        String expected = "&flyFrom=" + from + "&to=" + DEFAULT_TO;
        assertEquals(expected,result);
    }

    @Test
    void constructURLDateValid() {
        String expected = "&dateFrom=" + dateFrom + "&dateTo=" + dateTo;
        String result = service.constructURLDate(dateFrom, dateTo);
        assertEquals(expected, result);
    }

    @Test
    void constructURLDateInvalid() {
        String expected = "&dateTo=" + dateTo;
        String result = service.constructURLDate("30/10/10", dateTo);
        assertEquals(expected, result);
    }
}