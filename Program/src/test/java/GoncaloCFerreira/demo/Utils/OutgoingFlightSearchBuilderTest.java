package GoncaloCFerreira.demo.Utils;

import GoncaloCFerreira.demo.Model.FlightSearch;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.Flight;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.IncomingFlightSearch;
import GoncaloCFerreira.demo.Model.OutgoingFlightSearch.Bags;
import GoncaloCFerreira.demo.Model.OutgoingFlightSearch.OutgoingFlightSearch;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OutgoingFlightSearchBuilderTest {

    private final String search_id = "123-123";
    private final String currency = "EUR";
    private final String from = "OPO";
    private final String to = "LIS";
    private final String dateFrom = "30/01/1999";
    private final String dateTo = "30/02/1999";

    private final FlightSearch flightSearch = new FlightSearch(search_id, currency, from, to, dateFrom, dateTo);

    private final String flight_id1 = "123-123";
    private final List<String> airlines = new ArrayList<>(Arrays.asList("FR", "TP"));
    private final String flyFrom1 = "OPO";
    private final String flyTo1 = "LIS";
    private final double price1 = 103.53;
    private final double bag1_price1 = 23.40;
    private final double bag2_price1 = 52.34;
    private final Flight flight1 = new Flight(flight_id1, airlines, flyFrom1, flyTo1, price1, bag1_price1, bag2_price1);

    private final String flight_id2 = "321-323";
    private final String flyFrom2 = "LIS";
    private final String flyTo2 = "OPO";
    private final double price2 = 164.53;
    private final double bag1_price2 = 43.40;
    private final double bag2_price2 = 32.34;
    private final Flight flight2 = new Flight(flight_id2, airlines, flyFrom2, flyTo2, price2, bag1_price2, bag2_price2);

    private final String flight_id3 = "321-323";
    private final String flyFrom3 = "LIS";
    private final String flyTo3 = "OPO";
    private final double price3 = 23.12;
    private final double bag1_price3 = 43.43;
    private final double bag2_price3 = 0.0;

    private final Flight flight3 = new Flight(flight_id3, airlines, flyFrom3, flyTo3, price3, bag1_price3, bag2_price3);

    private final List<Flight> data = new ArrayList<>(Arrays.asList(flight1, flight2, flight3));
    private final IncomingFlightSearch ifs = new IncomingFlightSearch(flightSearch, data);

    private final double price_average = (price1 + price2 + price3) / 3;
    private final int numberOfFlights = 3;
    private final double bag1_price = (bag1_price1 + bag1_price2 + bag1_price3) / 3;
    private final double bag2_price = (bag2_price1 + bag2_price2) / 2;

    private final OutgoingFlightSearch ofs = new OutgoingFlightSearch(flightSearch, price_average, bag1_price, bag2_price, numberOfFlights);

    @Test
    void build() {
        OutgoingFlightSearch expectedResult = OutgoingFlightSearchBuilder.build(ifs);
        assertEquals(expectedResult, ofs);
    }
}