package GoncaloCFerreira.demo.Utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValueUtilsTest {

    @Test
    void round1() {
        double input = 2.00432;
        double output = ValueUtils.round(input);
        assertEquals(2.00, output);
    }

    @Test
    void round2() {
        double input = 2.0199;
        double output = ValueUtils.round(input);
        assertEquals(2.01, output);
    }

    @Test
    void validateDateCorrect() {
        String input = "30/01/1999";
        assertTrue(ValueUtils.validateDate(input));
    }

    @Test
    void validateDateIncorrectNull() {
        String input = null;
        assertFalse(ValueUtils.validateDate(input));
    }

    @Test
    void validateDateIncorrectValues() {
        String input = "30/30/1999";
        assertFalse(ValueUtils.validateDate(input));
    }

    @Test
    void validateDateIncorrectOrder() {
        String input = "30/2004/12";
        assertFalse(ValueUtils.validateDate(input));
    }

    @Test
    void stringifyDate() {
        String input = "30/01/1999";
        String output = ValueUtils.stringifyDate(input);
        assertEquals(output, "30\\/01\\/1999");
    }
}