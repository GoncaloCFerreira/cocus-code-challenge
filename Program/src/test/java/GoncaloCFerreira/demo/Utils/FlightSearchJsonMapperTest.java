package GoncaloCFerreira.demo.Utils;

import GoncaloCFerreira.demo.Model.FlightSearch;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.Flight;
import GoncaloCFerreira.demo.Model.IncomingFlightSearch.IncomingFlightSearch;
import GoncaloCFerreira.demo.Model.OutgoingFlightSearch.OutgoingFlightSearch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightSearchJsonMapperTest {

    private final FlightSearchJsonMapper flightMapper = new FlightSearchJsonMapper();

    String mockResponse = "{\n" +
            "    \"search_id\": \"f595192b-c525-4190-8ec6-89e7319f6b6c\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"id\": \"093e07f048b10000f7c33afe_0|07f0035848b200002eeb4b7b_0\",\n" +
            "            \"flyFrom\": \"OPO\",\n" +
            "            \"flyTo\": \"LIS\",\n" +
            "            \"airlines\": [\n" +
            "                \"FR\",\n" +
            "                \"TP\"\n" +
            "            ],\n" +
            "            \"price\": 103.53,\n" +
            "            \"bags_price\": {\n" +
            "                \"1\": 23.43,\n" +
            "                \"2\": 52.34\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": \"093e07f048b10000f7c33afe_0|07f0035848b200002eeb4b7b_0\",\n" +
            "            \"flyFrom\": \"LIS\",\n" +
            "            \"flyTo\": \"OPO\",\n" +
            "            \"airlines\": [\n" +
            "                 \"FR\",\n" +
            "                 \"TP\"\n" +
            "            ],\n" +
            "            \"price\": 164.53,\n" +
            "            \"bags_price\": {\n" +
            "                \"1\": 43.43,\n" +
            "                \"2\": 32.34\n" +
            "            }\n" +
            "        }\n" +
            "    ],\n" +
            "    \"currency\": \"EUR\",\n" +
            "    \"from\":\"OPO\",\n" +
            "    \"to\":\"LIS\",\n" +
            "    \"dateFrom\":\"30/01/2021\",\n" +
            "    \"dateTo\":\"30/02/2021\"\n" +
            "}";

    String mockJsonOutput = "{\"currency\":\"EUR\",\"from\":\"OPO\",\"to\":\"LIS\",\"dateFrom\":\"30/01/1999\",\"dateTo\":\"30/02/1999\",\"price_average\":134.03,\"number_of_flights\":2,\"bagPrices\":{\"bag1_average\":33.42,\"bag2_average\":42.34}}";

    private final String search_id = "f595192b-c525-4190-8ec6-89e7319f6b6c";
    private final String currency = "EUR";
    private final String from = "OPO";
    private final String to = "LIS";
    private final String dateFrom = "30/01/1999";
    private final String dateTo = "30/02/1999";

    private final FlightSearch flightSearch = new FlightSearch(search_id, currency, from, to, dateFrom, dateTo);

    private final String flight_id1 = "093e07f048b10000f7c33afe_0|07f0035848b200002eeb4b7b_0";
    private final List<String> airlines = new ArrayList<>(Arrays.asList("FR", "TP"));
    private final String flyFrom1 = "OPO";
    private final String flyTo1 = "LIS";
    private final double price1 = 103.53;
    private final double bag1_price1 = 23.43;
    private final double bag2_price1 = 52.34;
    private final Flight flight1 = new Flight(flight_id1, airlines, flyFrom1, flyTo1, price1, bag1_price1, bag2_price1);

    private final String flight_id2 = "093e07f048b10000f7c33afe_0|07f0035848b200002eeb4b7b_0";
    private final String flyFrom2 = "LIS";
    private final String flyTo2 = "OPO";
    private final double price2 = 164.53;
    private final double bag1_price2 = 43.43;
    private final double bag2_price2 = 32.34;

    private final Flight flight2 = new Flight(flight_id2, airlines, flyFrom2, flyTo2, price2, bag1_price2, bag2_price2);
    private final List<Flight> data = new ArrayList<>(Arrays.asList(flight1, flight2));
    private final IncomingFlightSearch ifs = new IncomingFlightSearch(flightSearch, data);

    private final IncomingFlightSearch ifsInvalid = new IncomingFlightSearch(flightSearch, data.subList(0,1));


    @Test
    void constructJsonToObjectValid() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode input = mapper.readTree(mockResponse);
        assertEquals(ifs, flightMapper.constructJsonToObject(input));
    }

    @Test
    void constructJsonToObjectDifferent() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode input = mapper.readTree(mockResponse);
        assertNotEquals(ifsInvalid, flightMapper.constructJsonToObject(input));
    }

    @Test
    void constructObjectToJsonValid() {
        OutgoingFlightSearch outgoing = OutgoingFlightSearchBuilder.build(ifs);
        assertEquals(mockJsonOutput,flightMapper.constructObjectToJson(outgoing));
    }

    @Test
    void constructObjectToJsonInvalid() {
        OutgoingFlightSearch outgoing = OutgoingFlightSearchBuilder.build(ifs);
        String expected = mockJsonOutput.replace("EUR","AUD");
        assertNotEquals(expected,flightMapper.constructObjectToJson(outgoing));
    }
}