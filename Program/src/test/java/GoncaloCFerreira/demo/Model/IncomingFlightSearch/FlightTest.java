package GoncaloCFerreira.demo.Model.IncomingFlightSearch;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightTest {

    private final String flight_id1 = "123-123";
    private final List<String> airlines = new ArrayList<>(Arrays.asList("FR", "TP"));
    private final String flyFrom1 = "OPO";
    private final String flyTo1 = "LIS";
    private final double price1 = 103.53;
    private final double bag1_price1 = 23.43;
    private final double bag2_price1 = 52.34;
    private final Flight flight1 = new Flight(flight_id1, airlines, flyFrom1, flyTo1, price1, bag1_price1, bag2_price1);
    private final Flight flight1Clone = new Flight(flight_id1, airlines, flyFrom1, flyTo1, price1, bag1_price1, bag2_price1);

    private final String flight_id2 = "321-323";
    private final String flyFrom2 = "LIS";
    private final String flyTo2 = "OPO";
    private final double price2 = 164.53;
    private final double bag1_price2 = 43.43;
    private final double bag2_price2 = 32.34;

    private final Flight flight2 = new Flight(flight_id2, airlines, flyFrom2, flyTo2, price2, bag1_price2, bag2_price2);

    @Test
    void getPrice() {
        assertEquals(flight1.getPrice(),price1);
    }

    @Test
    void getBag1_price() {
        assertEquals(flight1.getBag1_price(),bag1_price1);
    }

    @Test
    void getBag2_price() {
        assertEquals(flight1.getBag2_price(),bag2_price1);
    }

    @Test
    void testEquals() {
        assertNotEquals(flight1, flight2);
        assertEquals(flight1, flight1Clone);
        assertEquals(flight1,flight1);
        assertNotEquals(flight1,null);
        assertNotEquals(flight1,new String("Different class"));
    }

    @Test
    void getFlight_id() {
        assertEquals(flight_id1,flight1.getFlight_id());
    }

    @Test
    void getAirlines() {
        assertEquals(airlines,flight1.getAirlines());
    }

    @Test
    void getFlyFrom() {
        assertEquals(flyFrom1,flight1.getFlyFrom());
    }

    @Test
    void getFlyTo() {
        assertEquals(flyTo1,flight1.getFlyTo());
    }
}