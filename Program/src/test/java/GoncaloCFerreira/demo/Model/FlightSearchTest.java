package GoncaloCFerreira.demo.Model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlightSearchTest {

    private final String search_id = "123-123";
    private final String currency = "EUR";
    private final String from = "OPO";
    private final String to = "LIS";
    private final String dateFrom = "30/01/1999";
    private final String dateTo = "30/02/1999";

    private final FlightSearch flightSearch = new FlightSearch(search_id, currency, from, to, dateFrom, dateTo);

    @Test
    void getSearch_id() {
        assertEquals(flightSearch.getSearch_id(),search_id);
    }

    @Test
    void getCurrency() {
        assertEquals(flightSearch.getCurrency(),currency);
    }

    @Test
    void getFrom() {
        assertEquals(flightSearch.getFrom(),from);
    }

    @Test
    void getTo() {
        assertEquals(flightSearch.getTo(),to);
    }

    @Test
    void getDateFrom() {
        assertEquals(flightSearch.getDateFrom(),dateFrom);
    }

    @Test
    void getDateTo() {
        assertEquals(flightSearch.getDateTo(),dateTo);
    }
}