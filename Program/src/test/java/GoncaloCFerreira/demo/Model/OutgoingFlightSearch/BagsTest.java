package GoncaloCFerreira.demo.Model.OutgoingFlightSearch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BagsTest {

    private final double bag1_price = 33.2;
    private final double bag2_price = 51.5;
    private final Bags bags = new Bags(bag1_price,bag2_price);
    private final Bags bags2 = new Bags(bag2_price,bag1_price);
    private final Bags bagsClone = new Bags(bag1_price,bag2_price);

    @Test
    void getBag1_average() {
        assertEquals(bags.getBag1_average(),bag1_price);
    }

    @Test
    void getBag2_average() {
        assertEquals(bags.getBag2_average(),bag2_price);
    }

    @Test
    void testEquals() {
        assertNotEquals(bags,bags2);
        assertEquals(bags,bagsClone);
    }

}