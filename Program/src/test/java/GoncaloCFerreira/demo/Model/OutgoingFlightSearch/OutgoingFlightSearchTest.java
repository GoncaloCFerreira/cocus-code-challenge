package GoncaloCFerreira.demo.Model.OutgoingFlightSearch;

import GoncaloCFerreira.demo.Model.FlightSearch;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OutgoingFlightSearchTest {

    private final String search_id = "123-123";
    private final String currency = "EUR";
    private final String from = "OPO";
    private final String to = "LIS";
    private final String dateFrom = "30/01/1999";
    private final String dateTo = "30/02/1999";

    private final FlightSearch flightSearch = new FlightSearch(search_id, currency, from, to, dateFrom, dateTo);

    private final double price_average = 100.50;
    private final int number_of_flights = 10;
    private final double bag1_price = 33.2;
    private final double bag2_price = 51.54;
    private final Bags bags = new Bags(bag1_price,bag2_price);

    private final OutgoingFlightSearch ofs = new OutgoingFlightSearch(flightSearch,price_average,bag1_price,bag2_price,number_of_flights);
    private final OutgoingFlightSearch ofsClone = new OutgoingFlightSearch(flightSearch,price_average,bag1_price,bag2_price,number_of_flights);
    private final OutgoingFlightSearch ofs2 = new OutgoingFlightSearch(flightSearch,price_average,bag2_price,bag1_price,number_of_flights);


    @Test
    void getBagPrices() {
        Bags tmp = ofs.getBagPrices();
        assertEquals(tmp,bags);
    }

    @Test
    void getPrice_average() {
        assertEquals(ofs.getPrice_average(),price_average);
    }

    @Test
    void getNumberOfFlights() {
        assertEquals(ofs.getNumber_of_flights(),number_of_flights);
    }

    @Test
    void testEquals() {
        assertEquals(ofs,ofsClone);
        assertNotEquals(ofs,ofs2);
    }
}