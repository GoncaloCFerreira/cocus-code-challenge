package GoncaloCFerreira.demo.Integration;

import GoncaloCFerreira.demo.Utils.PropertiesReader;
import GoncaloCFerreira.demo.FlightInPortugalApplication;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class FlightInPortugalApplicationTests {

    private static String TESTING_URL;

    @BeforeAll
    public static void initiateTestServer() {
        Properties properties = null;
        try {
            properties = PropertiesReader.readPropertiesFile("application.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert properties != null;
        TESTING_URL = "http://localhost:" + properties.getProperty("TEST_SERVER_PORT");
        System.setProperty("server.servlet.context-path", "/flight");
        FlightInPortugalApplication.run(new String[]{}, "TEST");
    }

    @Test
    public void dateFromMissingWhenNoParameters() throws IOException {
        String url = TESTING_URL + "/flight/avg";
        URLConnection connection = new URL(url).openConnection();
        try (InputStream response = connection.getInputStream();
             Scanner scanner = new Scanner(response)) {
            String responseBody = scanner.nextLine();
            assertEquals("dateFrom parameter is missing (please use the format: dd/mm/yyyy)", responseBody);
        } catch (FileNotFoundException e) {
            fail("URL not found " + url);
        }
    }

    @Test
    public void dateToMissingWhen1Parameter() throws IOException {
        String url = TESTING_URL + "/flight/avg?dateFrom=30/01/2022";
        URLConnection connection = new URL(url).openConnection();
        try (InputStream response = connection.getInputStream();
             Scanner scanner = new Scanner(response)) {
            String responseBody = scanner.nextLine();
            assertEquals("dateTo parameter is missing (please use the format: dd/mm/yyyy)", responseBody);
        } catch (FileNotFoundException e) {
            fail("URL not found " + url);
        }
    }

    @Test
    public void dateToMissingWhen2ParameterWrong() throws IOException {
        String url = TESTING_URL + "/flight/avg?dateFrom=30/01/2022&dateTo=1/1/2001";
        URLConnection connection = new URL(url).openConnection();
        try (InputStream response = connection.getInputStream();
             Scanner scanner = new Scanner(response)) {
            String responseBody = scanner.nextLine();
            assertEquals("Invalid date formats (please use the format: dd/mm/yyyy) ", responseBody);
        } catch (FileNotFoundException e) {
            fail("URL not found " + url);
        }
    }

    @Test
    public void allDatesCorrect() throws IOException {
        String url = TESTING_URL + "/flight/avg?dateFrom=01/01/2021&dateTo=01/12/2021";
        URLConnection connection = new URL(url).openConnection();
        try (InputStream response = connection.getInputStream();
             Scanner scanner = new Scanner(response)) {
            String responseBody = scanner.nextLine();
            assertTrue(responseBody.contains("currency"));
        } catch (FileNotFoundException e) {
            fail("URL not found " + url);
        }
    }


}
