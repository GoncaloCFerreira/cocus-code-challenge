# COCUS Code Challenge

Made by: Gonçalo César Ferreira (goncalo_c_ferreira@hotmail.com)

Using Intellij IDE

# Usage

List of endpoints:

## /flight/list

- Request type = GET
- Lists all the records on the MongoDB database;
- No parameters

## /flight/deleteAll

- Request type = DELETE
- Deletes all the records on the MongoDB database;
- No parameters

## /flight/avg

- Request type = GET
- Results in the following output:
  - Currency used in the search and output values;
  - Location code From and To;
  - DateFrom and DateTo used in the search;
  - Average price of the flight;
  - Number of flights that where found;
  - Average price of the bags (bag one and bag two).
- Parameters:

  - dateFrom (dd/mm/yyyy) - mandatory;
  - dateTo (dd/mm/yyyy) - mandatory;
  - from (location code) - default configurable in the application.properties file;
  - to (location code) - default configurable in the application.properties file;

- The airlines used in the searches are configured in the application.properties file as well, by default TAP and Ryanair.

# Logic

- The model classes are primarily supporting the main FlightSearch object. This object contains the information that is used through the program, being the base class of two distinct classes:

    - IncomingFlightSearch - Which extends the FlightSearch object to contain the various Flights supplied by the external API.
    - OutgoingFlightSearch - Which is a processed and refined version of the previous one, with the resulting data that the client requested.

- This was done in order to preserve the integrity of the objects and make them as logical and understandable as possible (from the IncomingFlightSearch we can gather information to produce the OutgoingFlightSearch using the OutgoingFlightSearchBuilder).

- Two different mapping methods where used:

    - constructJsonToObject - Which maps the JSON String response of the external API into the model class IncomingFlightSearch;
    - constructObjectToJson - Which maps the model class OutgoingFlighSearch into a JSON String to be returned to the API Client.

- The different default constants are read from the application.properties file, located at /src/main/resources.

- The TravelService class contains the logic behind contacting the external API, forming the URL from the client's parameters as well as the properties variables (default URL head, default URL values, default URL tail, default URL currency).

- The Repository is produced by a Repository Factory, although only having one implementation of the Repository Interface as of this moment (MongoDB), this enables the expansion of the project if there is a need to add another type of repository in the future.

- In the beginning of every request the client makes, a entry is added to the MongoDB database with the following information:
    - Time stamp;
    - IP of the client;
    - Request that was used;
- It is possible to list and delete this information using the endpoints "/list" and "/deleteAll".

- A Logger class was created to take care of warnings and errors to be read by the administrator, every error in the application returns a simplified and readable response to the client.

# Extra information

- The project folder contains a HTML export of the coverage report provided by IntelliJ.

- Maven dependencies:
    - Junit - Testing;
    - Spring - Rest API connection and management;
    - Jackson - JSON management;
    - Mockito - To be used in the tests that access the external API (this implementation was not finished)
    - MongoDB - Non relational database for the management of requests to the API.
